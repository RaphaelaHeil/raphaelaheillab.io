---
title: Principles for HTR
event: Transkribus Workshop
event_url: https://abm.uu.se/cdhu/evenemang/event/eventdetail/?eventId=62722

location: 
address:
  street:
  city: Uppsala
  postcode: ''
  country: Sweden

summary: "Workshop organised by the Centre for Digital Humanities, Uppsala University"
abstract: ""


# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2021-10-13T13:15:00Z"
date_end: "2021-10-13T16:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2021-07-01T00:00:00Z"

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: true

#image:
#  caption: 'Image Credit: [Code Refinery](https://coderefinery.org/)'
#  focal_point: Right#

links:
- icon: clipboard
  icon_pack: fa
  name: Register
  url: https://abm.uu.se/cdhu/evenemang/event/eventdetail/?eventId=62722

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
- []

# Enable math on this page?
math: true
---
