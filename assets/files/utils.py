from urllib.request import urlretrieve
import zipfile
from pathlib import Path
from skimage.io import imread

def download():
    url = "https://app.box.com/index.php?rm=box_download_shared_file&shared_name=5rw2orqsbfmiwmuw5607vbg1eknw2k18&file_id=f_924189889942"
    urlretrieve(url, "out.zip")

def extract():
    with zipfile.ZipFile("out.zip","r") as zip_ref:
        zip_ref.extractall(".")

        
def get_dhnb_images(out_path:Path=Path("dida_single_digit_10k")):
    download()
    extract()
    if out_path.exists():
        print("Successfully donwloaded dataset!")
    else:
        print("Something has gone wrong, please check in with a helper")
    

def read_all_images(base_path:Path=Path("dida_single_digit_10k"), max_images_per_digit:int = 10):
    images = []
    labels = []
    ll = list(base_path.iterdir())
    ll.sort()
    for digit_dir in ll:
        image_counter = 0
        for filename in digit_dir.glob("*.jpg"):
            if image_counter < max_images_per_digit:
                images.append(imread(filename))
                labels.append(digit_dir.name)
                image_counter += 1
            else:
                break
    print("Loaded", len(images), "images from:", base_path)
    return images, labels
    
    
if __name__ == "__main__":
    get_dhnb_images()