+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 120  # Order that this section will appear.

title = "Extracurricular Activities"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[experience]]
  title = "Certified Carpentries Instructor"
  company = "The Carpentries"
  company_url = "https://carpentries.org/"
  location = ""
  date_start = "2020-05-20"
  date_end = ""
  description = """
  Volunteer instructor, teaching foundational coding and data science skills to researchers (e.g. Python, git, shell)

  Workshop contributions as instructor: 
  - March 2021: [SWC workshop](https://4turesearchdata-carpentries.github.io/2021-03-16-tudelft-online/) at Eindhoven University of Technology (held online)
  - June 2021: [SWC workshop](https://hdinkel.github.io/2021-06-14-mpikyb-online/) at Max Planck Institute for Biological Cybernetics (held online)
  - June 2021: [SWC workshop](https://4turesearchdata-carpentries.github.io/2021-06-21-Delft/) at Delft University of Technology (held online)
  - Upcoming: September 2021 SWC workshop at University of Bergen (held online)
  """

[[experience]]
  title = "Code Refinery Workshop Assistance"
  company = "Code Refinery"
  company_url = "https://coderefinery.org/"
  location = ""
  date_start = "2020-05-20"
  date_end = ""
  description = """
  Volunteer instructor and helper, teaching software management and development techniques to researchers (e.g. version control, documentation, reproducibility)

  Workshop contributions to date: 
  - November 2020: [Online CodeRefinery Workshop](https://coderefinery.github.io/2020-11-17-online/) as helper
  - September 2020: [Online CodeRefinery Workshop on Version Control](https://wikfeldt.github.io/2020-09-22-karlstad) for Karlstad University, SE, as instructor (part 1 of "Introduction to version control")
  - May 2020: [Online "Mega" CodeRefinery Workshop](https://coderefinery.github.io/2020-05-25-online), as helper
  """

[[experience]]
  title = "SSBAktuellt Editor"
  company = "Svenska Sällskapet för Automatiserad Bildanalys (SSBA) - Swedish Society for Automated Image Analysis"
  company_url = "https://ssba.org.se/"
  location = ""
  date_start = "2020-06-20"
  date_end = ""
  description = """
  SSBAktuellt is the biannual magazine of the Swedish Society for Automated Image Analysis (SSBA)

  Past issues are archived [here](http://ssba.org.se/nyhetsbrev/)
  """

+++
