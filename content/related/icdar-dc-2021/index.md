---
title: 'Links and Resources Related to the Poster "Computerised Image Processing for Handwritten Text Recognition of Historical Manuscripts"'
subtitle: 'presented at the ICDAR 2021 Doctoral Consortium'
summary: Links and Resources Related to the Poster "Computerised Image Processing for Handwritten Text Recognition of Historical Manuscripts" at ICDAR-DC 2021
authors:
- admin
tags:
categories:
- related
date: "2021-07-28T00:00:00Z"
lastmod: "2021-07-28T00:00:00Z"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
#image:
#  placement: 2
#  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
#  focal_point: ""
#  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---


### The Astrid Lindgren Code Project Website
[https://www.barnboksinstitutet.se/en/forskning/astrid-lindgren-koden/](https://www.barnboksinstitutet.se/en/forskning/astrid-lindgren-koden/)


### Strikethrough Removal from Handwritten Words Using CycleGANs
- will be presented at the poster session at ICDAR 2021
- code and data available here: [https://github.com/RaphaelaHeil/strikethrough-removal-cyclegans](https://github.com/RaphaelaHeil/strikethrough-removal-cyclegans)


### Author's Contact Details
[available here](../..) and at the bottom of this page
