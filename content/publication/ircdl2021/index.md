---
title: "Shorthand Secrets:
Deciphering Astrid Lindgren’s Stenographed
Drafts with HTR Methods"
authors:
- admin
- Malin Nauwerck
- Anders Hast
date: "2021-02-18T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *IRCDL 2021* (17th Italian Research Conference on Digital Libraries)
publication_short: In *IRCDL 2021*

abstract: "Astrid Lindgren, Swedish author of children’s books, is known for having both composed and edited her literary work in the Melin system of shorthand (a Swedish shorthand system based on Gabelsberger). Her original drafts and manuscripts are preserved in 670 stenographed notepads kept at the National Library of Sweden and The Swedish Institute of Children’s Books. For long these notepads have been considered undecipherable and are until recently untouched by research. <br>

This paper introduces handwritten text recognition (HTR) and docu ment image analysis (DIA) approaches to address the challenges inherent in Lindgren’s original drafts and manuscripts. It broadly covers aspects such as preprocessing and extraction of words, alignment of transcriptions and the fast transcription of large amounts of words.<br>

This is the first work to apply HTR and DIA to Gabelsberger-based shorthand material. In particular, it presents early-stage results which demonstrate that these stenographed manuscripts can indeed be transcribed, both manually by experts and by employing computerised approaches.
"

# Summary. An optional shortened abstract.
summary: "Early-stage results from _The Astrid Lindgren Code_ project"

tags:
- ''
featured: true

links:
url_pdf: http://ceur-ws.org/Vol-2816/short5.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
- []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
