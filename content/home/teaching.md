+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Teaching Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Maintenance Programming - TA"
  company = "Course Responsible: Johannes Borgström"
  company_url = ""
  location = ""
  date_start = "2018-09-03"
  date_end = "2018-10-28"
  description = """
  * Master level course
  * involvement in developing the lab assignments (various maintenance tasks surrounding a "legacy" system - in this case the Python to Java transpiler [VOC](https://github.com/beeware/voc))
  * lab assignment evaluation
  """

[[experience]]
  title = "Scientific Visualisation - TA"
  company = "Course Responsible: Anders Hast"
  company_url = ""
  location = ""
  date_start = "2018-09-03"
  date_end = "2018-10-28"
  description = """
  * Master level course
  * lab assistant
  * lab assignment evaluation
  """

[[experience]]
  title = "Advanced Software Design - TA"
  company = "Course Responsible: Francisco Fernández"
  company_url = ""
  location = ""
  date_start = "2018-10-29"
  date_end = "2019-01-20"
  description = """
  * Master level course
  * providing feedback on UML-based software designs
  """

[[experience]]
  title = "Scientific Visualisation - TA"
  company = "Course Responsible: Anders Hast, Alexandru Telea"
  company_url = "http://sese.nu/scientific-visualisation-2018/"
  location = ""
  date_start = "2018-11-26"
  date_end = "2018-11-30"
  description = """
  * PhD level course
  * lab assistant
  """

[[experience]]
  title = "User Interface Programming I - TA"
  company = "Course Responsible: Lars Oestreicher"
  company_url = ""
  location = ""
  date_start = "2019-01-21"
  date_end = "2019-03-24"
  description = """
  * Master level course
  * lab assistant
  * assisted in assessing student projects
  """

[[experience]]
  title = "Advanced Software Design - TA"
  company = "Course Responsible: Francisco Fernández"
  company_url = ""
  location = ""
  date_start = "2019-11-04"
  date_end = "2020-01-19"
  description = """
  * Master level course
  * providing feedback on UML-based software designs
  """

[[experience]]
  title = "Maintenance Programming - TA"
  company = "Course Responsible: Davide Vega D'aurelio"
  company_url = ""
  location = ""
  date_start = "2019-09-02"
  date_end = "2019-11-03"
  description = """
  * Master level course
  * lab assignment evaluation
  """

[[experience]]
  title = "User Interface Programming I - TA"
  company = "Course Responsible: Lars Oestreicher"
  company_url = ""
  location = ""
  date_start = "2020-01-20"
  date_end = "2020-03-22"
  description = """
  * Master level course
  * lab assistant (programming support)
  * assisted in assessing student projects
  """

[[experience]]
  title = "Master Thesis Supervision"
  company = ""
  company_url = ""
  location = ""
  date_start = "2020-01-20"
  date_end = "2020-06-30"
  description = """ During the spring semester 2020, I am supervising a Master student from the department of [Informatics and Media](https://im.uu.se/). More details about the thesis work and outcomes will be presented as a blog post as the work progresses. 
  """

[[experience]]
  title = "User Interface Programming I - TA"
  company = "Course Responsible: Lars Oestreicher"
  company_url = ""
  location = ""
  date_start = "2021-01-20"
  date_end = "2021-03-22"
  description = """
  * Master level course
  * assisted in assessing student projects
  """

[[experience]]
  title = "Maintenance Programming - TA"
  company = "Course Responsible: Cons T. Åhs"
  company_url = ""
  location = ""
  date_start = "2020-09-02"
  date_end = "2020-11-03"
  description = """
  * Master level course
  * full responsibility for creating and assessing the lab assignments 
  """


[[experience]]
  title = "Advanced Software Design - TA"
  company = "Course Responsible: Davide Vega D'aurelio"
  company_url = ""
  location = ""
  date_start = "2020-11-04"
  date_end = "2021-01-19"
  description = """
  * Master level course
  * held lecture about class diagrams
  * providing feedback on UML-based software designs
  """




+++
