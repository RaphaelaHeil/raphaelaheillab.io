+++
# About/Biography widget.

date = "2016-04-20T00:00:00"
draft = false

widget = "about"

# Order that this section will appear in.
weight = 1

author = "admin"

# List your academic interests.
[interests]
  interests = [
    "Handwritten Text Recognition",
    "Software Development",
    "Computer Graphics",
    "Machine Learning"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "PhD Studies in Computerised Image Processing (HTR of Historical Manuscripts)"
  institution = "[University Uppsala](http://www.uu.se/en)"
  year = "2018 - present"

[[education.courses]]
  course = "[M.Sc. Computer Science - Complex Software Systems](http://www.hs-bremen.de/internet/en/studium/stg/infmsc/index.html)"
  institution = "[Bremen University of Applied Sciences](http://www.hs-bremen.de)"
  year = 2017

[[education.courses]]
  course = "[B.Sc. Media Computer Science](http://www.hs-bremen.de/internet/en/studium/stg/mi/index.html)"
  institution = "[Bremen University of Applied Sciences](http://www.hs-bremen.de)"
  year = 2015

[[education.courses]]
  course = "Semester abroad"
  institution = "[University of Oslo, Norway](http://www.uio.no)"
  year = 2013

[[experience.places]]
  institution = "[Bremen University of Applied Sciences](http://www.hs-bremen.de) - Research project 'MadMAGS' (Markerless adaptive Mobile Augmented Reality in Games)"
  type = "Software Developer (image processing algorithms in C++)"
  fromDate = 2015-11-01
  toDate = 2017-02-28

[[experience.places]]
  institution = "European Organization for Nuclear Research ([CERN](http://home.cern/))"
  type = "Technical Studentship"
  fromDate = 2014-08-01
  toDate = 2015-08-31

[[experience.places]]
  institution = "[Bremen University of Applied Sciences](http://www.hs-bremen.de)"
  type = "Substitute maths tutor"
  fromDate = 2012-12-01
  toDate = 2013-01-31

[[experience.places]]
  institution = "[M2C Institute for Applied Media Technologies and Culture](http://www.m2c-bremen.de/)"
  type = "Software Developer (E-Learning platform in Java)"
  fromDate = 2012-08-01
  toDate = 2014-07-31

+++
